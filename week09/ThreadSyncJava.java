class MyThread1 extends Thread{
    synchronized public void run(){
       for(int i=1;i<=5;i++){
           System.out.println("2*"+i+"="+2*i);
       }
   }
}
class MyThread2 extends Thread{
   synchronized public void run(){
       for(int i=1;i<=5;i++){
           System.out.println("3*"+i+"="+3*i);
       }
   }
}
class Main extends Thread{
   public static void main(String[] args){
       MyThread1 obj1=new MyThread1();
       obj1.start();
       MyThread2 obj2=new MyThread2();
       obj2.start();    
   }
}

