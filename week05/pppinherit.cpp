// public inheritence
#include<iostream>
using namespace std;
class parent {
    private:
     int z = 7;
     int getpvt()
       {
           return z;
       }
    protected :
     int y = 8;
     public:
     int x = 9;

};
class publicinherit : public parent{
    public:
    int getpub(){
        return x;
        }
   
    int getprot(){
        return y;
        }
    
};
class protectedinherit : protected parent{
    public:
    int getpub(){
        return x;
        }
        void print()
        {
            cout<<"hi";
        }
    
    int getprot(){
        return y;
        }
    
};
class privateinherit : private parent{
    public:
    int getpub(){
        return x;
        }
   
    int getprot(){
        return y;
        }
    
};
int main(){
    publicinherit obj;
    cout<<"PUBLIC INHERITENCE : \n";
    cout<<"Public : "<<obj.getpub()<<endl;
    cout<<"Private can't be accessed "<<endl;
    cout<<"Protected : "<<obj.getprot()<<endl;
    protectedinherit objprot;
    cout<<"\nPROTECTED INHERITENCE : \n";
    cout<<"Public : "<<objprot.getpub()<<endl;
    cout<<"Private can't be accessed "<<endl;
    cout<<"Protected : "<<objprot.getprot()<<endl;
    objprot.print();
    privateinherit objpvt;
    cout<<"\nPRIVATE INHERITENCE : \n";
    cout<<"Public : "<<objpvt.getpub()<<endl;
    cout<<"Private can't be accessed "<<endl;
    cout<<"Protected : "<<objpvt.getprot()<<endl;
    return 0;
}

