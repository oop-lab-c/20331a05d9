//Program to demonstrate Template Functions and classes in C++.
#include<iostream>
using namespace std;
template<typename type>

type add(type a,type b) //defining a function template
{
    return a+b;
}

template<class type> //defining a class template
class Demo1
{
    type a;
    public:
    Demo1(type x)//constructor of type template
    {
        a=x;
    }
    type display()
    {
        return a;
    }
};

template<class type1,class type2> //defining another class template
class Demo2
{
    type1 a;
    type2 b;
    public:
    Demo2(type1 x,type2 y) //constructor to assign values
    {
        a=x;
        b=y;
    }
    void Sum()
    {
        cout<<"Result after adding "<<a<<" and "<<b<<" is "<<a+b<<endl;
    }
};

int main()
{
    int i1,i2;
    float f1,f2;
    string s1,s2;
    //Taking input from user
    cout<<"Enter two integers :"<<endl;
    cin>>i1>>i2;
    cout<<"Enter two floating point numbers :"<<endl;
    cin>>f1>>f2;
    cout<<"Enter 2 strings:"<<endl;
    cin>>s1>>s2;

    //calling template function with parameters of different datatypes
    cout<<"Demonstration of template functions "<<endl;
    cout<<add<float>(f1,f2)<<endl;
    cout<<add<int>(i1,i2)<<endl;
    cout<<add<string>(s1,s2)<<endl;

    cout<<"\nDemonstration of template class "<<endl;
    //instatiation with parameters of different datatypes
    Demo1<int>obj1(i1);
    Demo1<float>obj2(f1);
    Demo1<string>obj3(s1);
    //calling the methods of template class with parameters of different datatypes
    cout<<"Integer entered is : "<<obj1.display()<<endl;
    cout<<"Floating point number entered is : "<<obj2.display()<<endl;
    cout<<"String entered is : "<<obj3.display()<<endl;

    cout<<"\nDemonstration of template class "<<endl;
    //instatiation with parameters of different datatypes
    Demo2<int,int>obj4(i1,i2);
    Demo2<float,float>obj5(f1,f2);
    Demo2<int,float>obj6(i1,f1);
    Demo2<string,string>obj7(s1,s2);
    //calling the methods of template class with parameters of different datatypes
    obj4.Sum();
    obj5.Sum();
    obj6.Sum();
    obj7.Sum();
    return 0;
}